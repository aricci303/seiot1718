package seiot.consegna02;

import javax.swing.SwingUtilities;

class SmartGaragetMain   {

	static SmartGarageView view = null;
	static LogView log = null;

	public static void main(String[] args) throws Exception {	
		SwingUtilities.invokeAndWait(() -> {
			view = new SmartGarageView();
			log = new LogView();
		});
		Controller contr = new Controller("/dev/tty.usbmodem1411",view,log);
		view.registerController(contr);
		SwingUtilities.invokeLater(() -> {
			view.setVisible(true);
			log.setVisible(true);
		});
	}
}