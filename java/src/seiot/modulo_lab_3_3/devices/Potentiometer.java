package seiot.modulo_lab_3_3.devices;

public interface Potentiometer {
	
	int getValue();

}
