package seiot.consegna03;

import java.io.*;

class MessageViewer  {
	
	FileWriter writer;
	
	public MessageViewer(String logFile) throws IOException {
		writer = new java.io.FileWriter(new File(logFile));
	}
	
	public void log(String msg){
		synchronized(System.out){
			System.out.println(msg);
		}
		try {
			writer.write(msg+"\n");
			writer.flush();
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
}
