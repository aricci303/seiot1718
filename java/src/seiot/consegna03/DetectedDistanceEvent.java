package seiot.consegna03;

import seiot.modulo_lab_3_3.common.Event;

public class DetectedDistanceEvent implements Event {

	private double dist;
	
	public DetectedDistanceEvent(double dist){
		this.dist = dist;
	}
	
	public double getDistance(){
		return dist;
	}
}
