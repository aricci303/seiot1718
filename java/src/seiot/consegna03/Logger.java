package seiot.consegna03;

import java.util.Date;

class Logger  {

	public void log(String msg){
		String date = new Date().toString();
		synchronized(System.out){
			System.out.println("["+date+"] "+ msg +"\n");
		}
	}
}
