package seiot.consegna03;

import javax.swing.SwingUtilities;

import seiot.modulo_lab_2_2.msg.SerialCommChannel;

class SmartRadarMain   {

	public static void main(String[] args) throws Exception {	
		/*
		if (args.length != 1){
			System.err.println("Args: <serial port>");
			System.exit(1);
		}*/
		Logger logger = new Logger();
		String port = "/dev/cu.usbmodem1421";
		
		try {			
			MessageViewer viewer = new MessageViewer("log.txt");
			SerialCommChannel channel = new SerialCommChannel(port,9600);		

			RadarControllerAgent controller = new RadarControllerAgent(channel,viewer);
			controller.start();
			
			new MsgBrokerAgent(channel,controller,viewer,logger).start();
							
			/* attesa necessaria per fare in modo che Arduino completi il reboot */
			
			System.out.println("Waiting Arduino for rebooting...");		
			Thread.sleep(2000);
			System.out.println("Ready.");	
			
		} catch (Exception ex){
			ex.printStackTrace();
			logger.log("Exeption: "+ex.getMessage());
		}
	}
}