package seiot.consegna03;

import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import seiot.modulo_lab_2_2.msg.SerialCommChannel;
import seiot.modulo_lab_3_3.common.*;
import seiot.modulo_lab_3_3.devices.*;

public class RadarControllerAgent extends BasicEventLoopController {

	private static final String  MSG_REQUEST_ON = "0";
	private static final String  MSG_REQUEST_TO_MOVE = "1";
	private static final String  MSG_REQUEST_DETECT  = "2";
	private static final String  MSG_REQUEST_OFF   = "3";

	private static final double DETECTED_DIST = 0.5;	
	private static final double MIN_DIST = 0.2;
	
	private static final int ANGULAR_STEP = 10; /* radar angular step - fixed */
	private static final double MAX_SPEED = 45; /* degrees per sec */
	private static final double MIN_SPEED = 1;
	
	private Light ledOn, ledDetected, ledTracking;
	private ObservableButton onButton, offButton;
	private ObservablePotentiometer speedPot;
	private ObservableTimer timer;
	private SerialCommChannel channel;
	private MessageViewer viewer;	

	private enum State {OFF, SCANNING, MOVING_AND_DETECTING, TRACKING};
    private State state;

    
	private double angularSpeed; 		/* angular speed omega */
	private int currentAngle;	/* current angle - 0..180 */	
	private int angularStep;	/* angular step */
	private int objectCount;	

	private double timeStep;
	private Executor exec = Executors.newSingleThreadExecutor();
	
	public RadarControllerAgent(SerialCommChannel channel, MessageViewer viewer) throws Exception {
		this.channel = channel;
		this.viewer = viewer;
		state = State.OFF;
		timer = new ObservableTimer();
		timer.addObserver(this);
		ledOn = new seiot.modulo_lab_3_3.devices.emu.LedWithGUI(3,"ON");
		ledOn.switchOff();
		ledDetected = new seiot.modulo_lab_3_3.devices.emu.LedWithGUI(4,"DETECTED");
		ledDetected.switchOff();
		ledTracking = new seiot.modulo_lab_3_3.devices.emu.LedWithGUI(5,"TRACKING");
		ledTracking.switchOff();
		speedPot = new seiot.modulo_lab_3_3.devices.emu.ObservablePotentiometer(0, "speed",50);		
		onButton = new seiot.modulo_lab_3_3.devices.emu.ObservableButton(7, "ON");
		offButton = new seiot.modulo_lab_3_3.devices.emu.ObservableButton(8, "OFF");
		onButton.addObserver(this);
		offButton.addObserver(this);
		speedPot.addObserver(this);
	}
	
	@Override
	protected void processEvent(Event ev) {
		try {
		
			/* events that are processed in the same way in spite of the specific state */
			
			if (ev instanceof PotValueChanged){
				angularSpeed = MIN_SPEED + (MAX_SPEED - MIN_SPEED)*speedPot.getValue()/100;
				timeStep = ANGULAR_STEP*1000/angularSpeed;
				if (state != State.OFF){
					timer.stop();
			    	timer.start((int)timeStep);
				}
			} else if (ev instanceof ButtonPressed && ((ButtonPressed) ev).getSourceButton() == offButton){			    	
	    		channel.sendMsg(MSG_REQUEST_OFF);
		    	timer.stop();
	    		state = State.OFF;
	    		ledOn.switchOn();
	    		ledTracking.switchOff();
	    		ledDetected.switchOff();
	    		log("OFF");
	    	}
			
			// log("perceived event: "+ev);
			switch (state){
		    case OFF:
		    	if (ev instanceof ButtonPressed && ((ButtonPressed) ev).getSourceButton() == onButton){
		    		ledOn.switchOn();
			    	channel.sendMsg(MSG_REQUEST_ON);
			    	sleep(500);
					angularSpeed = MIN_SPEED + (MAX_SPEED - MIN_SPEED)*speedPot.getValue()/100;
					timeStep = ANGULAR_STEP*1000/angularSpeed;
		    		angularStep = ANGULAR_STEP;
		    		currentAngle = 0;
		    		objectCount = 0;
		    		timer.start((int)timeStep);
		    		state = State.SCANNING; 
		    		log("START SCANNING");
		    	} 

		    	break;
		    case SCANNING:
		    	if (ev instanceof Tick){
			    	channel.sendMsg(MSG_REQUEST_TO_MOVE+currentAngle);
			    	currentAngle += angularStep;
			    	if (currentAngle > 180){
			    		viewer.log("Total object detected: "+objectCount);
			    		objectCount = 0;
			    		currentAngle = 180;
			    		angularStep = -ANGULAR_STEP;
			    	} else if (currentAngle < 0){
			    		viewer.log("Total object detected: "+objectCount);
			    		objectCount = 0;
			    		currentAngle = 0;
			    		angularStep = ANGULAR_STEP;
			    	}
			    	state = State.MOVING_AND_DETECTING;
		    	} 
		    	break;
		    case MOVING_AND_DETECTING:
		    	if (ev instanceof DetectedDistanceEvent){
		    		DetectedDistanceEvent dev = (DetectedDistanceEvent) ev;
		    		double dist = dev.getDistance();
		    		log("current angle: "+currentAngle+" - distance detected: "+dist);
		    		if (dist < MIN_DIST){
			    		objectCount++;
		    			log("Object at "+dist+": start stracking");
		    			ledTracking.switchOn();
		    			state = State.TRACKING;
		    		} else if (dist < DETECTED_DIST){
			    		objectCount++;
		    			exec.execute(() -> {
		    				try {
			    				ledDetected.switchOn();
				    			sleep(100);
				    			ledDetected.switchOff();
		    				} catch (Exception ex){};
		    			});
		    			String time = new Date().toString().substring(11,18);
			    		viewer.log("Time "+time +" - Object detected at angle "+currentAngle);
			    		state = State.SCANNING;
		    		} else {
			    		state = State.SCANNING;
		    		}
		    	}
		    	break;
		    case TRACKING:
		    	if (ev instanceof Tick){
			    	channel.sendMsg(MSG_REQUEST_DETECT);
		    	} else if (ev instanceof DetectedDistanceEvent){
		    		DetectedDistanceEvent dev = (DetectedDistanceEvent) ev;
		    		double dist = dev.getDistance();
		    		// log("TRACKING DISTANCE: "+dist);
	    			String time = new Date().toString().substring(11,18);
		    		viewer.log("Time "+time +" - Object tracked at angle "+currentAngle+" distance "+dist);
		    		if (dist > MIN_DIST){
		    			ledTracking.switchOff();
		    			state = State.SCANNING;
		    		} 
		    	}
		    	break;
		  }
		} catch (Exception ex){
			ex.printStackTrace();
		}		
	}

	private void log(String msg){
		System.out.println("[RadarController] "+msg);
	}
	
}
