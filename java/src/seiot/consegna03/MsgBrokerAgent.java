package seiot.consegna03;

import seiot.modulo_lab_2_2.msg.*;

public class MsgBrokerAgent extends Thread {

	SerialCommChannel channel;
	MessageViewer view;
	RadarControllerAgent controller;
	Logger logger;
	
	static final String MSG_DISTANCE  	= "1|";
	
	public MsgBrokerAgent(SerialCommChannel channel, RadarControllerAgent controller, MessageViewer view, Logger log) throws Exception {
		this.view = view;
		this.logger = log;
		this.channel = channel;
		this.controller = controller;
	}
	
	public void run(){
		while (true){
			try {
				String msg = channel.receiveMsg();
				if (msg.startsWith("[L")){
					logger.log(msg.substring(4));
				} else if (msg.startsWith(MSG_DISTANCE)){
					try {
						double dist = Double.parseDouble(msg.substring(2));
						controller.notifyEvent(new DetectedDistanceEvent(dist));
					} catch (Exception ex){
						logger.log("wrong msg distance: "+msg);
					}
				}
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}

}
