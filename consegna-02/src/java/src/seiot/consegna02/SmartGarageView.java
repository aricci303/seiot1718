package seiot.consegna02;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

class SmartGarageView extends JFrame  {

	private JTextField state;

	private JButton enter;
	private JButton stopCar;

	private JTextField distance;
	private JTextField warns;
	private JTextField info;

	long lastWarnTime;

	public SmartGarageView(){
		super("Car View");
		setSize(400,140);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));

		mainPanel.add(Box.createRigidArea(new Dimension(20,0)));

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));	    
		enter = new JButton("Enter");
		buttonPanel.add(enter);
		stopCar = new JButton("Stop");
		buttonPanel.add(stopCar);	    
		mainPanel.add(buttonPanel);
		
		mainPanel.add(Box.createRigidArea(new Dimension(20,0)));
		
		JPanel outputPanel = new JPanel();
		outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.Y_AXIS));	    

		JPanel infoLine = new JPanel();
		infoLine.setLayout(new BoxLayout(infoLine, BoxLayout.LINE_AXIS));
		infoLine.add(new JLabel("Info"));
		info = new JTextField(60);
		info.setPreferredSize(new Dimension(200,15));
		info.setEditable(false);
		infoLine.add(info);
		outputPanel.add(infoLine);

		JPanel distanceLine = new JPanel();
		distanceLine.setLayout(new BoxLayout(distanceLine, BoxLayout.LINE_AXIS));
		distanceLine.add(new JLabel("Distance: "));
		distance = new JTextField(60);
		distance.setPreferredSize(new Dimension(200,15));
		distance.setEditable(false);
		distanceLine.add(distance);
		outputPanel.add(distanceLine);

		JPanel warnLine = new JPanel();
		warnLine.setLayout(new BoxLayout(warnLine, BoxLayout.LINE_AXIS));
		warnLine.add(new JLabel("Warnings"));
		warns = new JTextField(60);
		warns.setPreferredSize(new Dimension(200,15));
		warns.setEditable(false);
		warnLine.add(warns);
		outputPanel.add(warnLine);

		mainPanel.add(outputPanel);

		this.getContentPane().add(mainPanel);

		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent ev){
				System.exit(-1);
			}
		});
	}

	public void registerController(Controller contr){
		enter.addActionListener(contr);
		stopCar.addActionListener(contr);
	}

	public void setDistance(String msg){
		SwingUtilities.invokeLater(() -> {
			distance.setText(msg);
		});
	}

	public void setInfo(String msg){
		SwingUtilities.invokeLater(() -> {
			info.setText(msg);
		});
	}

	public void setWarn(String msg){
		SwingUtilities.invokeLater(() -> {
			warns.setText(msg);
			lastWarnTime = System.currentTimeMillis();
			/* remove the warn after 1 sec */
			new Thread(() -> {
				try {
					Thread.sleep(1000);	
					if (System.currentTimeMillis() - lastWarnTime >= 1000){
						SwingUtilities.invokeLater(() -> {
							warns.setText("");
						});
					}
				} catch (Exception ex){}
			}).start();
		});
	}

	
}
