package seiot.consegna02;

import seiot.modulo_lab_2_2.msg.*;

public class MonitoringAgent extends Thread {

	SerialCommChannel channel;
	SmartGarageView view;
	LogView logger;
	
	static final String MSG_WELCOME 	=  "1";
	static final String MSG_DISTANCE  	= "2|";
	static final String MSG_CAN_STOP  	= "3";
	static final String MSG_STOP_OK   	= "4";
	static final String MSG_TOO_FAR   	= "5";
	static final String MSG_TOUCHING  	= "6";
	static final String MSG_APPROACHING = "7";
	static final String MSG_GONE        = "8";
	
	public MonitoringAgent(SerialCommChannel channel, SmartGarageView view, LogView log) throws Exception {
		this.view = view;
		this.logger = log;
		this.channel = channel;
	}
	
	public void run(){
		while (true){
			try {
				String msg = channel.receiveMsg();
				if (msg.startsWith("[L")){
					logger.log(msg.substring(4));
				} else if (msg.startsWith(MSG_WELCOME)){
					view.setInfo("Welcome Home");
					view.setDistance("");
				} else if (msg.startsWith(MSG_DISTANCE)) {
					view.setDistance(msg.substring(2));
				} else if (msg.startsWith(MSG_CAN_STOP)) {
					view.setInfo("OK CAN STOP");
					view.setDistance("");
				} else if (msg.startsWith(MSG_STOP_OK)) {
					view.setInfo("OK");
					view.setDistance("");
				} else if (msg.startsWith(MSG_TOO_FAR)) {
					view.setWarn("TOO FAR");
				} else if (msg.startsWith(MSG_TOUCHING)) {
					view.setWarn("TOUCHING");
				} else if (msg.startsWith(MSG_APPROACHING)) {
					view.setInfo("APPROACHING");
				} else if (msg.startsWith(MSG_GONE)) {
					view.setInfo("");
					view.setDistance("");
				}
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}

}
