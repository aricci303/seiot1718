#ifndef __PARKING_LED_SYSTEM__
#define __PARKING_LED_SYSTEM__

#include "LedExt.h"

class ParkingLedSystem { 
public:
  	ParkingLedSystem();
  	void setLevel(float level);
protected:
	LedExt* ldist1;	
	LedExt* ldist2;	
};

#endif
