#include "Car.h"

#define MSG_WELCOME   "1"
#define MSG_DISTANCE  "2|"
#define MSG_CAN_STOP  "3"
#define MSG_STOP_OK   "4"
#define MSG_TOO_FAR   "5"
#define MSG_TOUCHING  "6"
#define MSG_APPROACHING  "7"
#define MSG_GONE        "8"


Car::Car(){
	arriving = false;
	stopRequested = false;
	parked = false;
  readyToPark = false;
  gone = false;
  currentDistance = DIST_GONE + 1;
}

void Car::setCurrentDistance(float d){
  currentDistance = d;
}

float Car::getCurrentDistance(){
  return currentDistance;  
}

bool Car::checkArriving(){
  bool wasArriving = arriving;
  arriving = false;
	return wasArriving;
}

bool Car::checkStopRequest(){
  bool stop = stopRequested;
  stopRequested = false;
  return stop;
}

bool Car::checkGone(){
  bool isGone = gone;
  gone = false;
  return isGone;
}

bool Car::checkReadyToPark(){
  bool ready = readyToPark;
  readyToPark = false;
  return ready;
}

bool Car::checkParked(){
  bool isParked = parked;
  parked = false;
  return isParked;
}

void Car::setArriving(){
	arriving = true;
}

void Car::setStopRequest(){
  stopRequested = true;
}

void Car::setReadyToPark(){
  readyToPark = true;
}

void Car::setParked(){
  parked = true;
}

void Car::setGone(){
  MsgService.sendMsg(MSG_GONE);
  gone = true;
}

void Car::sendWelcomeMsg(){
  MsgService.sendMsg(MSG_WELCOME);
}

void Car::sendDistance(){
  MsgService.sendMsg(MSG_DISTANCE+String(currentDistance));
}

void Car::sendCanStop(){
  MsgService.sendMsg(MSG_CAN_STOP);
}

void Car::sendApproaching(){
  MsgService.sendMsg(MSG_APPROACHING);
}

void Car::sendTooFar(){
  MsgService.sendMsg(MSG_TOO_FAR);
}

void Car::sendStopOK(){
  MsgService.sendMsg(MSG_STOP_OK);
}

void Car::sendTouching(){
  MsgService.sendMsg(MSG_TOUCHING);
}




