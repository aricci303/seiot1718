#ifndef __GARAGE_CONTROL_TASK__
#define __GARAGE_CONTROL_TASK__

#include "Task.h"
#include "Car.h"
#include "PresenceSensor.h"
#include "LedExt.h"
#include "Button.h"

class GarageControlTask: public Task {

public:
  GarageControlTask(Car* pCar);
  void init(int period);  
  void tick();

private:
	Car* pCar;
	PresenceSensor* pDetector;
	Button* pClose;

	LedExt* lr;
	int doorOpeningTime;
  int arrivalTime;

  enum { IDLE, OPENING, OPEN, CLOSING, PARKING } state;
};

#endif

