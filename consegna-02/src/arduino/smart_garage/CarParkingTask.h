#ifndef __CAR_PARKING_TASK__
#define __CAR_PARKING_TASK__

#include "Task.h"
#include "Car.h"
#include "ProximitySensor.h"
#include "ParkingLedSystem.h"
#include "Button.h"

class CarParkingTask: public Task {

public:

  CarParkingTask(Car* pCar);
  void init(int period);  
  void tick();
  
private:
  float fetchDistance();
  
  Car* pCar;  
  ProximitySensor* pProx;
  ParkingLedSystem* pLeds;
  Button* pTouch;
  
  float lastDistance;
  
  enum { IDLE, FAR, APPROACHING, CAN_STOP } state;
};

#endif

