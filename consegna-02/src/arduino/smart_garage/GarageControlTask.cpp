#include "GarageControlTask.h"
#include "config.h"
#include "ButtonImpl.h"
#include "Pir.h"
#include "MsgService.h"
#include "Logger.h"

#define ARRIVAL_TIMEOUT 10000
#define DOOR_DURATION 2000

GarageControlTask::GarageControlTask(Car* pCar){
  this->pCar = pCar;
  pDetector = new Pir(PIR_PIN);
  lr = new LedExt(LR_PIN);
  lr->switchOn();
  pClose = new ButtonImpl(CLOSE_PIN);
}
  
void GarageControlTask::init(int period){
  Task::init(period);
  state = IDLE;
  lr->setIntensity(0);
  Logger.log("GCT:INIT");
}
  
void GarageControlTask::tick(){
  switch (state){
    case IDLE: {
      if (pCar->checkArriving()){
        doorOpeningTime = 0;
        arrivalTime = 0;
        state = OPENING;          
        Logger.log("GCT:IDLE -> OPENING");
      }
      break;
    }
    case OPENING: {
      // MsgService.sendMsg("OPENING");
      doorOpeningTime += myPeriod;
      arrivalTime += myPeriod;
      if (doorOpeningTime > DOOR_DURATION){
        lr->setIntensity(255);
        state = OPEN;  
        Logger.log("GCT:OPENING -> OPEN");
      } else {
        int level = map(doorOpeningTime,0,DOOR_DURATION,0,255);
        lr->setIntensity(level);
      }
      break;
    }
    case OPEN: {
      arrivalTime += myPeriod;
      bool carDetected = pDetector->isDetected();
      bool timeout = arrivalTime > ARRIVAL_TIMEOUT;
      bool closeReq = pClose->isPressed();
      if (closeReq && pCar->getCurrentDistance() < DIST_CLOSE){
        doorOpeningTime = 0; 
        state = CLOSING;  
        Logger.log("GCT:OPEN -> CLOSING");
      } else if (carDetected){
        pCar->sendWelcomeMsg();
        pCar->setReadyToPark();
        state = PARKING;
        Logger.log("GCT:OPEN|CAR DETECTED -> PARKING");
      } else if (timeout){
        doorOpeningTime = 0; 
        state = CLOSING;  
        Logger.log("GCT:OPEN|TIMEOUT -> CLOSING");
      }
      break;
    }
    case PARKING: {
      bool closeReq = pClose->isPressed();
      if (closeReq && pCar->getCurrentDistance() < DIST_CLOSE){
        doorOpeningTime = 0; 
        state = CLOSING;  
        Logger.log("GCT:PARKING|CLOSE -> CLOSING");
      } else if (pCar->checkParked() || pCar->checkGone()){
        doorOpeningTime = 0;
        state = CLOSING;
        Logger.log("GCT:PARKING|GONE or PARKED -> CLOSING");
      } 
      break;
    }
    case CLOSING: {
      doorOpeningTime += myPeriod;
      if (doorOpeningTime > DOOR_DURATION){
        lr->setIntensity(0);
        Logger.log("GCT:CLOSING -> IDLE");
        state = IDLE;  
      } else {
        int level = map(doorOpeningTime,0,DOOR_DURATION,0,255);
        lr->setIntensity(255-level);
      }
      break;
    }
  }
}
