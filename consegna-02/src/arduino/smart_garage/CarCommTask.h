#ifndef __CAR_COMM_TASK__
#define __CAR_COMM_TASK__

#include "Task.h"
#include "Car.h"

class CarCommTask: public Task {

public:

  CarCommTask(Car* pCar);
  void init(int period);  
  void tick();

  /* sent to the car */

private:
  Car* pCar;
};

#endif

