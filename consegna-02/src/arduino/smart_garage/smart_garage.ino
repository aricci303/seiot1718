/*
 * SEIOT a.a. 2017/2018
 *
 * CONSEGNA #2 - SMART GARAGE
 *
 * author: A. Ricci
 *
 */
#include "Scheduler.h"
#include "Car.h"
#include "CarCommTask.h"
#include "GarageControlTask.h"
#include "CarParkingTask.h"
#include "MsgService.h"

Scheduler sched;

void setup(){
  
  sched.init(50);

  MsgService.init();
    
  Car* pCar = new Car();

  CarCommTask* pCarComm = new CarCommTask(pCar);
  pCarComm->init(50);
  sched.addTask(pCarComm);

  CarParkingTask* pCarParking = new CarParkingTask(pCar);
  pCarParking->init(50);
  sched.addTask(pCarParking);

  GarageControlTask* pGarageControl = new GarageControlTask(pCar);
  pGarageControl->init(50);
  sched.addTask(pGarageControl);
}

void loop(){
  sched.schedule();
}
