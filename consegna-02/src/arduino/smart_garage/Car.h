#ifndef __CAR__
#define __CAR__

#include "MsgService.h"

#define DIST_MAX    0.60 
#define DIST_MIN    0.20
#define DIST_CLOSE  0.40
#define DIST_GONE   1.00

class Car {
public:

  Car();

  bool checkArriving();
  bool checkReadyToPark();
  bool checkStopRequest();
  bool checkGone();
  bool checkParked();

  void setReadyToPark();
  void setArriving();
  void setStopRequest();
  void setParked();
  void setGone();

  void sendWelcomeMsg();
  void sendDistance();
  void sendCanStop();
  void sendTooFar();
  void sendStopOK();
  void sendTouching();
  void sendApproaching();

  float getCurrentDistance();
  void setCurrentDistance(float distance);

private:
  bool stopRequested;
  bool readyToPark;
  bool arriving;
  bool parked;
  bool gone;
  float currentDistance;
};

#endif

