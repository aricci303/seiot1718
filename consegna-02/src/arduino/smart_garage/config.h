#ifndef __CONFIG__
#define __CONFIG__

#define LDIST1_PIN 3
#define LDIST2_PIN 5
#define LR_PIN 6
#define TOUCH_PIN 4
#define CLOSE_PIN 2
#define PROX_TRIG_PIN 7
#define PROX_ECHO_PIN 8
#define PIR_PIN 12

#endif
