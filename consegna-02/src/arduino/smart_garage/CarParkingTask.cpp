#include "CarParkingTask.h"
#include "Sonar.h"
#include "ButtonImpl.h"
#include "config.h"
#include "Logger.h"

#define DIST_SPIKE 50

CarParkingTask::CarParkingTask(Car* pCar){
  this->pCar = pCar;
  pLeds = new ParkingLedSystem();
  pProx = new Sonar(PROX_ECHO_PIN,PROX_TRIG_PIN);
  pTouch = new ButtonImpl(TOUCH_PIN);
}
  
void CarParkingTask::init(int period){
  Task::init(period);
  state = IDLE;
  pLeds->setLevel(0);
  Logger.log("CPT:INIT");
}
  
void CarParkingTask::tick(){
  switch (state){
    case IDLE: {
      if (pCar->checkReadyToPark()){
        state = FAR;
        pLeds->setLevel(0);
        Logger.log("CPT:FAR");
      }
      break;
    }
    case FAR: {
      float distance = fetchDistance();
      pCar->setCurrentDistance(distance);
      if (distance < DIST_MAX){
        pCar->sendApproaching();
        state = APPROACHING;
        Logger.log("CPT:APPROACHING");
      } else {
        if (pCar->checkStopRequest()){
          pCar->sendTooFar();   
          Logger.log("CPT:TOOFAR");
        }
        if (distance > DIST_GONE){
          Logger.log("CPT:GONE");
          pCar->setGone();
          state = IDLE;    
        }
      } 
      break;
    }
    case APPROACHING: {
      float distance = fetchDistance();
      pCar->setCurrentDistance(distance);
      pCar->sendDistance();
      if (distance > DIST_MAX){
        pLeds->setLevel(0);
        state = FAR;
        Logger.log("CPT:APPROACHING => FAR");
      } else if (distance < DIST_MIN){
        pLeds->setLevel(1);
        state = CAN_STOP;
        Logger.log("CPT:APPROACHING => CAN STOP");
      } else {
        float level = (distance - DIST_MIN)/ (DIST_MAX - DIST_MIN);
        pLeds->setLevel(1-level);
      }

      if (pCar->checkStopRequest()){
        if (distance > DIST_CLOSE){
          Logger.log("CPT:APPROACHING | TOO FAR");
          pCar->sendTooFar();   
        } else {
          pLeds->setLevel(0);
          pCar->sendStopOK();
          pCar->setParked();
          state = IDLE;        
          Logger.log("CPT:APPROACHING - PARKED");
        }
      }
      break;
    }
    case CAN_STOP: {
      pCar->sendCanStop();
      float distance = fetchDistance();
      pCar->setCurrentDistance(distance);
      pCar->sendDistance();
      if (distance > DIST_MIN){
        Logger.log("CPT:CAN_STOP|far -> APPROACHING");
        state = APPROACHING;
      } else if (pCar->checkStopRequest()){
        pCar->sendStopOK();
        pCar->setParked();
        pLeds->setLevel(0);
        Logger.log("CPT:CAN_STOP|stop -> PARKED");
        state = IDLE;        
      } 

      if (pTouch->isPressed()){
        pCar->sendTouching();
        Logger.log("CPT:TOUCHING");
      }
      break;
    }
  }
}

float CarParkingTask::fetchDistance(){
  float distance = pProx->getDistance();
  if (distance - lastDistance > DIST_SPIKE){
    distance = pProx->getDistance();
  } 
  lastDistance = distance;
  return distance;
}

