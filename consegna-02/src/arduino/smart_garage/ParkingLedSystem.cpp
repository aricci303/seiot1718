#include "ParkingLedSystem.h"
#include "Arduino.h"
#include "config.h"

ParkingLedSystem::ParkingLedSystem(){
  ldist1 = new LedExt(LDIST1_PIN);
  ldist2 = new LedExt(LDIST2_PIN);
  ldist1->switchOn();
  ldist2->switchOn();
}

void ParkingLedSystem::setLevel(float level){
  if (level < 0.5){
  	ldist2->setIntensity(0);	
  	ldist1->setIntensity(level*2*255);	
  }	else {
  	ldist1->setIntensity(255);	
  	ldist2->setIntensity((level-0.5)*2*255);	
  }
}

