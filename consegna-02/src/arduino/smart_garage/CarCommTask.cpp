#include "CarCommTask.h"
#include "config.h"
#include "Logger.h"

#define MSG_ENTER_REQUEST "1"
#define MSG_STOP_REQUEST "2"

CarCommTask::CarCommTask(Car* pCar){
  this->pCar = pCar;

}
  
void CarCommTask::init(int period){
  Task::init(period);
  Logger.log("CCT:Init");
}
  
void CarCommTask::tick(){
  if (MsgService.isMsgAvailable()){
    Msg* msg = MsgService.receiveMsg(); 
    const String& content = msg->getContent();
    if (content == MSG_ENTER_REQUEST){
      pCar->setArriving();
      Logger.log("CCT: enter request.");
    } else if (content == MSG_STOP_REQUEST){
      pCar->setStopRequest();
      Logger.log("CCT: stop request.");
    } else {
      Logger.log("CCT: unknown req: "+content);
    }
    delete msg;    
  }
}


