#include "RadarTask.h"
#include "Sonar.h"
#include "config.h"
#include "MsgService.h"
#include "Logger.h"
#include <Servo.h>

#define MSG_REQUEST_ON      "0"
#define MSG_REQUEST_TO_MOVE "1"
#define MSG_REQUEST_DETECT  "2"
#define MSG_REQUEST_OFF     "3"

#define MSG_DISTANCE "1|"

RadarTask::RadarTask(){
  pProx = new Sonar(PROX_ECHO_PIN,PROX_TRIG_PIN);
  pConnected = new Led(LED_CONNECTED_PIN);
}
  
void RadarTask::init(int period){
  Task::init(period);
  state = OFF;
  pServo->attach(9);
  Logger.log("RT:INIT");
}
  
void RadarTask::tick(){
  switch (state){
    case OFF: {
      if (MsgService.isMsgAvailable()){
        Msg* msg = MsgService.receiveMsg(); 
        const String& content = msg->getContent();
        if (content.startsWith(MSG_REQUEST_ON)){
          Logger.log("RT|OFF - on");
          pConnected->switchOn();
          pServo->write(0);         
          state = IDLE;
        } else {
          Logger.log("RT|OFF - unknown msg");
        }
        delete msg;
      }
      break;      
    }
    case IDLE: {
      if (MsgService.isMsgAvailable()){
        Msg* msg = MsgService.receiveMsg(); 
        const String& content = msg->getContent();
        if (content.startsWith(MSG_REQUEST_TO_MOVE)){
          angle = content.substring(1).toInt();
          Logger.log("RT|IDLE - move to "+angle);
          state = MOVING;
        } else if (content.startsWith(MSG_REQUEST_DETECT)){
          Logger.log("RT|IDLE - detecting");
          state = DETECTING;
        } else if (content.startsWith(MSG_REQUEST_OFF)){
          Logger.log("RT|IDLE - reset");
          state = RESET;
        }
        delete msg;
      }
      break;
    }
    case MOVING: {
      pServo->write(angle);         
      // delay(1); // TO BE REMOVED
      Logger.log("RT|MOVING - moving done ");
      state = DETECTING;
      break;
    }
    case DETECTING: {
      float distance = pProx->getDistance();
      const String& dist = String(distance);
      MsgService.sendMsg(MSG_DISTANCE+dist);
      Logger.log("RT|DETECTING - fetched distance: "+dist);
      state = IDLE;
      break;
    }
    case RESET: {
      pServo->write(90);         
      // delay(25); // TO BE REMOVED
      Logger.log("RT|RESET - reset done ");
      state = OFF;
      break;
    }
  }
}


