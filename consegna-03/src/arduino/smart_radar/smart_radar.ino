/*
 * SEIOT a.a. 2017/2018
 *
 * CONSEGNA #3 - SMART RADAR
 *
 * author: A. Ricci
 *
 */
#include "Scheduler.h"
#include "MsgService.h"
#include "RadarTask.h"

Scheduler sched;

void setup(){
  
  sched.init(20);

  MsgService.init();
    
  RadarTask* pRadarTask = new RadarTask();
  pRadarTask->init(10);
  sched.addTask(pRadarTask);
}

void loop(){
  sched.schedule();
}
