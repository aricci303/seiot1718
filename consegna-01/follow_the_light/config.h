/*
 *  This header file stores symbols that concerns 
 *  the configuration of the system.
 *
 */
#ifndef __CONFIG__
#define __CONFIG__

#define LED01_PIN 4
#define LED02_PIN 5
#define LED03_PIN 6

#define BUT01_PIN 8
#define BUT02_PIN 9
#define BUT03_PIN 10

#define PULSELED_PIN 3

#define POT_PIN A0

#define MAX_USER_DELAY 10000
#define MIN_USER_DELAY 1000

#define MAX_SEQ_LEN 256

#define SINGLE_LED_ON_TIME 250
#define LED_OFF_TIME 100

#define DT_GAME_OVER 2500
#endif

