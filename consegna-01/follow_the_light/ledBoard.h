/*
 * This module is responsible of the led board
 *
 */
#ifndef __LEDBOARD__
#define __LEDBOARD__

#define END_OF_SEQ 1

void initLedBoard();   
void resetSeq();
void animateSeq(); 
int updateSeq();
byte currentSeqLen();
bool matchSeq(byte* seqRead);
void pulseLed();
void flashLed();

#endif

