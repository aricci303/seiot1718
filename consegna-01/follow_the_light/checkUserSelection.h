/*
 * This module is responsible of controlling the 
 * selected led and checking user selection and
 * the termination of game over  
 *
 */
#ifndef __USERSEL__
#define __USERSEL__

void initUserInput();
byte* readUserSeq(int len, long maxDelayUserSelection);
void dumpSeq(byte* seq, int currLen);

#endif

