#include "Arduino.h"
#include "config.h"
#include "ledBoard.h"

int ledPin[] = {LED01_PIN, LED02_PIN, LED03_PIN};

/* where the sequence is stored */
byte sequence[MAX_SEQ_LEN];

/* current sequence length */
byte seqLen;

void initLedBoard(){   
  for (int i = 0; i < 3; i++){
    pinMode(ledPin[i], OUTPUT);     
  }
  pinMode(PULSELED_PIN,OUTPUT);  
}

void resetSeq(){
  randomSeed(analogRead(A1));
  seqLen = 0;
}

void animateSeq(){  
  for (byte i = 0; i < seqLen; i++){
    byte ledIndex = sequence[i];
    digitalWrite(ledPin[ledIndex],HIGH);
    delay(SINGLE_LED_ON_TIME);
    digitalWrite(ledPin[ledIndex],LOW);
    delay(LED_OFF_TIME);
  }
}

int updateSeq(){
  if (seqLen < MAX_SEQ_LEN){  
    seqLen++;    
    sequence[seqLen] = (byte) random(0,3);
    return 0; 
  } else {
    return END_OF_SEQ;        
  }
}

byte currentSeqLen(){
  return seqLen;
}

bool matchSeq(byte* seqRead){
  byte i = 0;
  while (i < seqLen && seqRead[i] == sequence[i])
    i++;
  return i == seqLen;
}


void pulseLed(){
  int currIntensity = 0;
  int fadeAmount = 5;
  int pressed = digitalRead(BUT01_PIN);
  while (pressed == LOW){    
    analogWrite(PULSELED_PIN, currIntensity);   
    currIntensity = currIntensity + fadeAmount;
    if (currIntensity == 0 || currIntensity == 255) {
      fadeAmount = -fadeAmount ; 
    }     
    pressed = digitalRead(BUT01_PIN);
    delay(20);
  }  
  analogWrite(PULSELED_PIN, 0);   
}

void flashLed(){
    analogWrite(PULSELED_PIN, 255);   
    delay(DT_GAME_OVER);
    analogWrite(PULSELED_PIN, 0);     
}


