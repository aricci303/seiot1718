#include "Arduino.h"
#include "config.h"

#define NO_PRESSED -1

int buttonPin[] = {BUT01_PIN, BUT02_PIN, BUT03_PIN};
byte seqRead[MAX_SEQ_LEN];

void initUserInput(){
  for (int i = 0; i < 3; i++){
    pinMode(buttonPin[i], INPUT);     
  } 
}

static int getPressed(){
  for (int i = 0; i < 3; i++){
    if (digitalRead(buttonPin[i])==HIGH){
      return i;  
    }
  }  
  return -1;
}

byte* readUserSeq(int len, long maxDelayUserSelection){
  int nReads = 0;
  long currentTime = 0;  
  while (nReads < len && currentTime < maxDelayUserSelection){      
    int pressed = getPressed();
    long t0 = millis(); 
    while (pressed == NO_PRESSED && currentTime < maxDelayUserSelection){
      pressed = getPressed();
      long t1 = millis();
      currentTime = currentTime + (t1-t0);
      t0 = t1;
    }  
    if (pressed != NO_PRESSED){
      seqRead[nReads] = (byte) pressed;
      delay(200);
      nReads++;
    }
  }
  if (nReads == len){
    return seqRead;   
  } else {
    return NULL;
  }
}

void dumpSeq(byte* seq, int currLen){
  Serial.println("SEQ:");
  for (int i = 0; i < currLen; i++){
    Serial.println(seq[i]);     
  }
}


