/*
 * SEIOT a.a. 2017/2018
 *
 * CONSEGNA #1 - FOLLOW THE LIGHT GAME
 *
 * Esempio di soluzione.
 */
#include "config.h"
#include "ledBoard.h"
#include "checkUserSelection.h"

 
long maxDelayUserSelection; 
long score; 
bool timeout;
bool wrongSeq;
double bonus;
  
void setup() {
  initLedBoard();
  initUserInput();
  Serial.begin(9600);
}

void loop(){

  /* initial stage */ 
  Serial.println(".:: WELCOME to Follow the Light! ::.");
  pulseLed();

  /* compute max user delay and bonus */
  double ledSpeed = analogRead(POT_PIN);
  maxDelayUserSelection = (int)(ledSpeed / 1023 * (MAX_USER_DELAY - MIN_USER_DELAY)+MIN_USER_DELAY);
  bonus = 2 - (ledSpeed / 1023);
  
  /* start the game */ 
  Serial.println("Ready!");
  Serial.println("You have max "+String(maxDelayUserSelection) + "ms to insert the sequence!");
  Serial.println("Your bonus will be: "+String(bonus));
  delay(1000);

  timeout = false;
  wrongSeq = false;
  score = 0;
  resetSeq();
  int endOfSeq = updateSeq();
  
  /* main game loop */
  
  while (!timeout && !wrongSeq && !endOfSeq){
    byte currLen = currentSeqLen();
    Serial.print("Sequence #"+String(currLen)+"...");
    animateSeq();
    byte* seq = readUserSeq(currLen,maxDelayUserSelection);
    if (seq){
      // dumpSeq(seq,currLen);
      if (matchSeq(seq)) {
        Serial.println("GREAT!");
        score += currLen;
        endOfSeq = updateSeq();
        delay(1000);
      } else {
        wrongSeq = true;
        Serial.println("OUCH");
      }
    } else {
      timeout = true;
      Serial.println("TIME OUT!");
    }
  }

  /* game over */ 
  score = score * bonus;
  Serial.println("GAME OVER - Final score: "+String(score)+"\n");  
  flashLed();
}
